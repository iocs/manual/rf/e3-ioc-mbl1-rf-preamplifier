require ecmccfg 8.0.0
require essioc

## Load plugin:
require pmu905

## Initiation:
epicsEnvSet("IOC" ,"$(IOC="MBL-010RFC:Ctrl-CC-001")")
epicsEnvSet("ECMCCFG_INIT" ,"")  #Only run startup once (auto at PSI, need call at ESS), variable set to "#" in startup.cmd
epicsEnvSet("SCRIPTEXEC" ,"$(SCRIPTEXEC="iocshLoad")")

# run module startup.cmd (only needed at ESS  PSI auto call at require)
$(ECMCCFG_INIT)$(SCRIPTEXEC) ${ecmccfg_DIR}startup.cmd, "IOC=$(IOC),ECMC_VER=8.0.0,MASTER_ID=-1"

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

##############################################################################
## Configure hardware:
# No EtherCAT hardware (in this example)..

##############################################################################
## Load plugin:
epicsEnvSet(ECMC_PLUGIN_FILNAME,"${ecmc_socketcan_DIR}lib/${EPICS_HOST_ARCH=linux-x86_64}/libecmc_socketcan.so")
epicsEnvSet(ECMC_PLUGIN_CONFIG,"IF=can0;DBG_PRINT=0;") # Only one option implemented in this plugin
${SCRIPTEXEC} ${ecmccfg_DIR}loadPlugin.cmd, "PLUGIN_ID=0,FILE=${ECMC_PLUGIN_FILNAME},CONFIG='${ECMC_PLUGIN_CONFIG}', REPORT=1"
epicsEnvUnset(ECMC_PLUGIN_FILNAME)
epicsEnvUnset(ECMC_PLUGIN_CONFIG)


##############################################################################
## Communication diag
dbLoadRecords(ecmcPluginSocketCAN_Com.template, "P=${ECMC_PREFIX},PORT=${ECMC_ASYN_PORT},ADDR=0,TIMEOUT=1")

##############################################################################
############# Configure CANOpen plugin:

# Kvaser CANOpen USB is the Master (ID=0)
iocshLoad("${pmu905_DIR}/pmu905_master.iocsh", "ECMC_PREFIX=${ECMC_PREFIX},ECMC_ASYN_PORT=${ECMC_ASYN_PORT},ECMC_SAMPLE_RATE_MS=${ECMC_SAMPLE_RATE_MS},ECMC_TSE=${ECMC_TSE},DEV_ID=0")


# -----------------------------------------------------------------------------
# Configure PMU905 Nodes and adding support to their SDO:s and PDO:s
# -----------------------------------------------------------------------------
epicsEnvSet("CH_I" ,"00")
# MBL-010
iocshLoad("${pmu905_DIR}/rack_row.iocsh")
# MBL-020
iocshLoad("${pmu905_DIR}/rack_row.iocsh")
# MBL-030
iocshLoad("${pmu905_DIR}/rack_row.iocsh")
# MBL-040
iocshLoad("${pmu905_DIR}/rack_row.iocsh")
# MBL-050
iocshLoad("${pmu905_DIR}/rack_row.iocsh")
# MBL-060
#iocshLoad("${pmu905_DIR}/rack_row.iocsh")
# MBL-070
#iocshLoad("${pmu905_DIR}/rack_row.iocsh")
# MBL-080
#iocshLoad("${pmu905_DIR}/rack_row.iocsh")
# MBL-090
#iocshLoad("${pmu905_DIR}/rack_row.iocsh")


##############################################################################

############# Go to realtime:

ecmcConfigOrDie "Cfg.SetAppMode(1)"

iocInit()

